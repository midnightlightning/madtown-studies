var studySites = [];
var studySiteRegistryAbi = [
    {
        "constant": false,
        "inputs": [
            {
                "name": "index",
                "type": "uint256"
            }
        ],
        "name": "remLoc",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "name": "studySites",
        "outputs": [
            {
                "name": "exists",
                "type": "bool"
            },
            {
                "name": "numApprovals",
                "type": "uint256"
            },
            {
                "name": "isOutside",
                "type": "bool"
            },
            {
                "name": "location",
                "type": "string"
            },
            {
                "name": "name",
                "type": "string"
            },
            {
                "name": "environment",
                "type": "string"
            },
            {
                "name": "resources",
                "type": "string"
            },
            {
                "name": "description",
                "type": "string"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "index",
                "type": "uint256"
            },
            {
                "name": "name",
                "type": "string"
            },
            {
                "name": "description",
                "type": "string"
            },
            {
                "name": "location",
                "type": "string"
            }
        ],
        "name": "editLoc",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "name",
                "type": "string"
            },
            {
                "name": "description",
                "type": "string"
            },
            {
                "name": "isOutside",
                "type": "bool"
            },
            {
                "name": "location",
                "type": "string"
            },
            {
                "name": "environment",
                "type": "string"
            },
            {
                "name": "resources",
                "type": "string"
            }
        ],
        "name": "suggestLoc",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "toCheck1",
                "type": "string"
            },
            {
                "name": "toCheck2",
                "type": "string"
            }
        ],
        "name": "compareStrings",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "adminContract",
        "outputs": [
            {
                "name": "",
                "type": "address"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "getNumSites",
        "outputs": [
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "index",
                "type": "uint256"
            }
        ],
        "name": "approveLoc",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "name": "adminContractAddress",
                "type": "address"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "constructor"
    }
];
var hasMetamask = false;

$(document).ready(function() {
    console.log("ready!");
    var $warning = $('#metaMaskWarning').html("");
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
        hasMetamask = true;

        var StudySiteRegistry = web3.eth.contract(studySiteRegistryAbi);
        var studySiteInstance = StudySiteRegistry.at('0xdd07200d1de3f0b72881d92d939dc333de34a32d');
        studySiteInstance.getNumSites.call(function(err, rs) {
          if (err != null) {
            console.error(err);
            return;
          }
          var numStudySites = rs.toNumber();
          studySites = [];
          for (var i = 0; i < numStudySites; i++) {
            studySiteInstance.studySites.call(i, function(err, rs) {
              if (err != null) {
                console.error(err);
                return;
              }
              let resources = [];
              try {
                resources = JSON.parse(rs[6]);
              } catch (e) {
                // Do nothing
              }
              let numApproals = rs[1].toNumber();
              studySites.push({
                name: rs[4],
                numApprovals: numApproals,
                outin: rs[2] ? 'outside' : 'inside',
                environment: rs[5],
                resources: resources,
                address: 'https://w3w.co/' + rs[3]
              });
            });
          }
        });
    }
    if(hasMetamask == false) {
        $warning.append("<h3>ATTENTION: User is using demo data becacuse they don't have MetaMask installed.</h3>");
    }

    $("#submit").on("click", function(e){
        //Display results.
        var $result = $('#results').html("");
        if(studySites.length == 0) {
            $result.append("<h4>No places loaded.</h4>");
            return;
        }
        for(var i = 0; i < studySites.length; i++) {
            var $resultItem = $('<div></div>');
            var site = studySites[i];
            $resultItem.append('<h4><a href = "' + site.address + '" target = "_blank"> ' + site.name + '</a></h4>');
            $resultItem.append('<p class="environment"><strong>Environment</strong> ' + site.environment + '</p>');
            $resultItem.append('<p class="inout"><strong>Inside or Outside?</strong> ' + site.outin + '</p>');
            $resultItem.append('<p class="resources"><strong>Resources</strong> ' + site.resources.join(', ') + '</p>');
            $resultItem.append('<p class="approals"><strong>Admin Approvals</strong> ' + site.numApprovals + '</p>');
            $result.append($resultItem);
        }

    });
});
