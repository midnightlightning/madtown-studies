var adminContractAbi = [
  {
    "constant": true,
    "inputs": [],
    "name": "numAdmins",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "constructor"
  },
  {
    "constant": true,
    "inputs": [
      {
        "name": "toCheck",
        "type": "address"
      }
    ],
    "name": "isAdmin",
    "outputs": [
      {
        "name": "",
        "type": "bool"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": false,
    "inputs": [
      {
        "name": "toAdd",
        "type": "address"
      }
    ],
    "name": "addAdmin",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "constant": false,
    "inputs": [
      {
        "name": "toRemove",
        "type": "address"
      }
    ],
    "name": "remAdmin",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  }
];
var hasMetamask = false;


function paintUserDetails(userData) {
  var $details = $('#user-details').html('');
  $details.append('<h2>' + userData.address + '</h2>');
  if (userData.isAdmin) {
      $details.append('<span class="admin">Is project administrator</span>');
  }
}

$(document).ready(function() {
    console.log("ready!");
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
        hasMetamask = true;
    }

    function showUserDetails(userAddress) {
        var userData = {
          address: userAddress,
          isAdmin: false
        }
        if (!hasMetamask) {
          paintUserDetails(userData);
          return;
        }
        if (hasMetamask) {
          var AdminContract = web3.eth.contract(adminContractAbi);
          var adminContractInstance = AdminContract.at('0xebb69a6ccfe2ecd994b48fc658b38f8882479805');
          console.log('Fetching admin status for ', userAddress);
          console.log(web3.toHex(userAddress));
          adminContractInstance.isAdmin.call(userAddress, function(err, rs) {
            if (err != null) {
              console.error(err);
              return;
            }
            userData.isAdmin = rs;
            paintUserDetails(userData);
          });
        }
    }

    $(window).on('hashchange', function(e) {
        console.log( 'hash changed' );
        showUserDetails(location.hash.substr(1));
    });
    if (location.hash != '') {
        showUserDetails(location.hash.substr(1));
    }

    $('#user-lookup').on('click', function(e) {
        var userAddress = $('#address-input').val();
        location.hash = userAddress;
    });
});
