var studySites = [
{name:"Wendt Commons", food:"yes", environment:"collaborative", outin:"inside", resources:["wifi", "outlets", "availableComp", "caeComp", "desks", "whiteboards", "printers"], address:"https://w3w.co/shine.native.record"},
{name:"College Library", food:"yes", environment:"noisy", outin:"inside", resources:["wifi", "outlets", "desks", "whiteboards", "books", "foodToBuy", "scanners", "printers"], address:"https://w3w.co/hints.chest.jolly"},
{name:"Law Library", food:"no", environment:"quiet", outin:"inside", resources:["wifi", "outlets", "desks", "books", "scanners", "printers"], address:"https://w3w.co/spare.prep.oven"},
{name:"Starbucks", food:"no", environment:"noisy", outin:"inside", resources:["wifi", "outlets", "desks", "foodToBuy"], address:"https://w3w.co/groom.dads.juror"},
{name:"Memorial Union", food:"yes", environment:"noisy", outin:"both", resources:["wifi", "outlets", "desks", "foodToBuy"], address:"https://w3w.co/cage.flops.probe"},
{name:"St. Paul's", food:"yes", environment:"quiet", outin:"inside", resources:["wifi", "outlets", "desks"], address:"https://w3w.co/pure.feared.bunk"},
{name:"Ebling Library", food:"yes", environment:"quiet", outin:"inside", resources:["wifi", "outlets", "desks"], address:"https://w3w.co/trades.during.lofts"},
{name:"Steenbock", food:"yes", environment:"collaborative", outin:"inside", resources:["wifi", "outlets", "availableComp", "desks", "whiteboards", "books", "scanners", "printers"], address:"https://w3w.co/aware.person.fades"},
{name:"Engineering Hall", food:"yes", environment:"noisy", outin:"inside", resources:["wifi", "outlets", "availableComp", "caeComp", "linuxComp", "desks", "monitors", "foodToBuy", "scanners", "printers"], address:"https://w3w.co/target.mixer.kicks"},
{name:"1410 Engineering Drive", food:"yes", environment:"collaborative", outin:"inside", resources:["wifi", "outlets", "availableComp", "caeComp", "desks", "scanners", "printers"], address:"https://w3w.co/wash.lucky.burst"},
{name:"Bascom Hill", food:"yes", environment:"noisy", outin:"outside", resources:["foodToBuy"], address:"https://w3w.co/comet.drama.home"}
];
var studySiteRegistryAbi = [
    {
        "constant": false,
        "inputs": [
            {
                "name": "index",
                "type": "uint256"
            }
        ],
        "name": "remLoc",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "name": "studySites",
        "outputs": [
            {
                "name": "exists",
                "type": "bool"
            },
            {
                "name": "numApprovals",
                "type": "uint256"
            },
            {
                "name": "isOutside",
                "type": "bool"
            },
            {
                "name": "location",
                "type": "string"
            },
            {
                "name": "name",
                "type": "string"
            },
            {
                "name": "environment",
                "type": "string"
            },
            {
                "name": "resources",
                "type": "string"
            },
            {
                "name": "description",
                "type": "string"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "index",
                "type": "uint256"
            },
            {
                "name": "name",
                "type": "string"
            },
            {
                "name": "description",
                "type": "string"
            },
            {
                "name": "location",
                "type": "string"
            }
        ],
        "name": "editLoc",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "name",
                "type": "string"
            },
            {
                "name": "description",
                "type": "string"
            },
            {
                "name": "isOutside",
                "type": "bool"
            },
            {
                "name": "location",
                "type": "string"
            },
            {
                "name": "environment",
                "type": "string"
            },
            {
                "name": "resources",
                "type": "string"
            }
        ],
        "name": "suggestLoc",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "toCheck1",
                "type": "string"
            },
            {
                "name": "toCheck2",
                "type": "string"
            }
        ],
        "name": "compareStrings",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "adminContract",
        "outputs": [
            {
                "name": "",
                "type": "address"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "getNumSites",
        "outputs": [
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "index",
                "type": "uint256"
            }
        ],
        "name": "approveLoc",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "name": "adminContractAddress",
                "type": "address"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "constructor"
    }
];
var hasMetamask = false;

$(document).ready(function() {
    console.log("ready!");
    var $warning = $('#metaMaskWarning').html("");
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
        hasMetamask = true;

        var StudySiteRegistry = web3.eth.contract(studySiteRegistryAbi);
        var studySiteInstance = StudySiteRegistry.at('0xdd07200d1de3f0b72881d92d939dc333de34a32d');
        studySiteInstance.getNumSites.call(function(err, rs) {
          if (err != null) {
            console.error(err);
            return;
          }
          var numStudySites = rs.toNumber();
          studySites = [];
          for (var i = 0; i < numStudySites; i++) {
            studySiteInstance.studySites.call(i, function(err, rs) {
              if (err != null) {
                console.error(err);
                return;
              }
              let resources = [];
              try {
                resources = JSON.parse(rs[6]);
              } catch (e) {
                // Do nothing
              }
              let numApprovals = rs[1].toNumber();
              if (numApprovals >= 2) {
                studySites.push({
                  name: rs[4],
                  outin: rs[2] ? 'outside' : 'inside',
                  environment: rs[5],
                  resources: resources,
                  address: 'https://w3w.co/' + rs[3]
                });
              }
            });
          }
        });
    }
    if(hasMetamask == false) {
        $warning.append("<h3>ATTENTION: User is using demo data becacuse they don't have MetaMask installed.</h3>");
    }

    $("#submit").on("click", function(e){
        //Iterate through all form fields.
        var food = $('input:radio[name="food"]:checked').val();
        var environment = $("#Environment").val();
        var outin = $('input:radio[name="outinside"]:checked').val();
        var resources = $('input:checkbox[name="resources"]:checked').map(function() {
            return $(this).val();
        }).get();

        //Filter list of study sites with info.

        var searchResults = studySites.filter(function(studySite){
            var bool = true;
            if(studySite.food != food && food != "idk") {
                bool = false;
            }
            if(studySite.environment != environment) {
                bool = false;
            }
            if(studySite.outin != outin && studySite.outin != "both") {
                bool = false;
            }
            for(var i = 0; i < resources.length; i++) {
                var criteriaMet = false;
                for(var j = 0; j < studySite.resources.length; j++) {
                    if(resources[i] == studySite.resources[j]) {
                        criteriaMet = true;
                    }
                }
                if(criteriaMet == false && resources.length > 0) {
                    bool = false;
                }
            }

            return bool;
        });
        //Display results.
        var $result = $('#results').html("");
        if(searchResults.length == 0) {
            $result.append("<h4>No places found that meet that criteria.</h4>");
            return;
        }
        for(var i = 0; i < searchResults.length; i++) {
            var $resultItem = $('<div></div>');
            var site = searchResults[i];
            $resultItem.append('<h4><a href = "' + site.address + '" target = "_blank"> ' + site.name + '</a></h4>');
            $resultItem.append('<p class="environment"><strong>Environment</strong> ' + site.environment + '</p>');
            $resultItem.append('<p class="inout"><strong>Inside or Outside?</strong> ' + site.outin + '</p>');
            $resultItem.append('<p class="resources"><strong>Resources</strong> ' + site.resources.join(', ') + '</p>');
            $result.append($resultItem);
        }

        var placesToStudy = searchResults.map(function(studySite) {
            return studySite.name;
        });

    });
});
