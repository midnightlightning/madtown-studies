# Run development server
To get the Metamask browser plugin to detect properly, the files must be served from from a web server rather than the `file://` path.

Run once, to install globally:

```
npm install -g http-server
```

Then, to serve the static front-end files:

```
http-server ./
```

Then you can visit http://127.0.0.1:8080 to view the website.
