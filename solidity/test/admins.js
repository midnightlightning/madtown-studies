const Admins = artifacts.require('Admins');

contract('Admins', accounts => {
    it("should set contract creator as an admin", async function() {
        const admins = await Admins.deployed();
        assert.equal(
            (await admins.isAdmin.call(accounts[0])),
            true,
            "didn't assign appropriate address as owner"
        );
    });

    it("should add and remove an admin to the listing", async function() {
        const admins = await Admins.deployed();

        // initial check for not already Admin
        assert.equal(
            (await admins.isAdmin.call(accounts[1])),
            false,
            "already an Admin"
        );

        // add Admin to list
        await admins.addAdmin(accounts[1]);

        // verify the newly added Admin is in list
        assert.equal(
            (await admins.isAdmin.call(accounts[1])),
            true,
            "didn't add new address as Admin"
        );

        // remove Admin from list
        await admins.remAdmin(accounts[1]);

        // verify Admin actually removed from list
        assert.equal(
            (await admins.isAdmin.call(accounts[1])),
            false,
            "already an Admin"
        );
    });    

    it("should increment/decrement numAdmins as admins are added/removed", async function() {
        const admins = await Admins.deployed();
        let numAdmins = 1;

        // initial check numAdmins
        assert.equal(
            (await admins.numAdmins.call()),
            numAdmins,
            "initial Admin count wrong"
        );

        // addAdmin to list
        await admins.addAdmin(accounts[1]);
        numAdmins = numAdmins + 1; // previous action should have added new Admin

        // check numAdmins incremented
        assert.equal(
            (await admins.numAdmins.call()),
            numAdmins,
            "Admin count wrong after addAdmin"
        );

        // removeAdmin from list
        await admins.remAdmin(accounts[1]);
        numAdmins = numAdmins - 1; // previous action should have removed new Admin

        // check numAdmins decremented
        assert.equal(
            (await admins.numAdmins.call()),
            numAdmins,
            "Admin count wrong after removeAdmin"
        );
    });
});
